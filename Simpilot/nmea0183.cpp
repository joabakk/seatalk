/*
  Description:
    NMEA 0183 message implementations.
    Define NMEA0183 in port.h to compile.
    NMEA 0183 reference: http://freenmea.net/docs

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <string.h>
#include "simpilot.h"

#ifdef NMEA0183
namespace Simpilot {

  #define MaxSentence 256              // NMEA 0183 max sentence length
  #define TalkerID "EC"                // NMEA 0183 talker ID

  // GPS modes, as defined by NMEA 0183
  enum GpsMode {
    GpsModeNoFix = 0,
    GpsModeFix = 1,
    GpsModeDiffFix = 2,
    GpsModePPS = 3,
    GpsModeRTK = 4,
    GpsModeFloatRTK = 5,
    GpsModeEstimated = 6,
    GpsModeManual = 7,
    GpsModeSimulated = 8
  };

  static int computeChecksum(const char * sentence) {
    // compute a NMEA0183 checksum by XOR'ing all characters after the '$' sign
    int checksum = 0; 
    if (*sentence != '$') return 0; // invalid sentence
    for (const char *ptr = sentence + 1; *ptr != '\0'; ptr++) { 
      checksum ^= *ptr;
    }
    return checksum;
  }

  static char* appendChecksum(char * sentence) {
    // appends a checksum onto the given NMEA 0183 sentence
    int checksum = computeChecksum(sentence); // NB: don't include '*' in checksum computation
    strcat(sentence, "*");
    fmtHex(sentence + strlen(sentence), checksum);
    return sentence;
  }

  EnvMessage::EnvMessage() {
    depth = DefaultDepth;
  }
  
  void EnvMessage::send() {
    char sentence[MaxSentence];
    fmtString(sentence, "$%sDBT,%.1f,f,%.1f,M,%.1f,F", TalkerID, M_TO_FT(depth), depth, M_TO_FA(depth));
    outputLine(appendChecksum(sentence));
  }

  PositionMessage::PositionMessage() {
  }
  
  void PositionMessage::send() {
    char sentence[MaxSentence];
    // NMEA time format is hhmmss.ss
    int hrs, mins, secs, lat_deg, lng_deg;
    double lat_min, lng_min;
    char lat_dir, lng_dir;
    splitTime(time, &hrs, &mins, &secs);
    position.parts(&lat_deg, &lat_min, &lat_dir, &lng_deg, &lng_min, &lng_dir);
    fmtString(sentence, "$%sGGA,%02d%02d%02d.00,%02d%02.2f,%c,%03d%02.2f,%c,%d,%d,%01.1f,%01.1f,%c,%01.1f,%c,,%04d",
              TalkerID, hrs, mins, secs, lat_deg, lat_min, lat_dir, lng_deg, lng_min, lng_dir, GpsModeFix,
              GpsNumSatellites, GpsHorizontalDilution, GpsAntennaAltitude, Meters, GpsGeoidalSeparation, Meters, GpsDiffStationID);
    outputLine(appendChecksum(sentence));
  }

  VectorMessage::VectorMessage() {
    speed = 0;
    speed_units = Knots;
    heading = 0;
    mag_heading = 0;
  }
  
  void VectorMessage::send() {
    // NB: don't bother with a HDT message since VTG contains heading info
    char sentence[MaxSentence];
    double mps = Geo::toMps(speed, speed_units);
    double knots = Geo::fromMps(mps, Knots);
    double kph = Geo::fromMps(mps, Kph);
    fmtString(sentence, "$%sVTG,%.1f,T,%.1f,M,%.1f,N,%.1f,K", TalkerID, heading, mag_heading, knots, kph);
    outputLine(appendChecksum(sentence));
  }

  NavMessage::NavMessage() {
    xte_magnitude = 0.0;
    xte_units = NauticalMiles;
    steer = SteerRight;
    orig_id = "";
    dest_id = "";
    wpt_status = WaypointNotSet;
  }
  
  void NavMessage::send() {
    // ToDo: calculate XTE
    char sentence[MaxSentence];
    int lat_deg, lng_deg;
    double lat_min, lng_min;
    char lat_dir, lng_dir;
    dest.parts(&lat_deg, &lat_min, &lat_dir, &lng_deg, &lng_min, &lng_dir);
    fmtString(sentence, "$%sRMB,%c,%.1f,%c,%s,%s,%02d%02.2f,%c,%03d%02.2f,%c,%.1f,%.1f,%.1f,%c,",
              TalkerID, 'A', xte_magnitude, steer, dest_id, orig_id, 
              lat_deg, lat_min, lat_dir, lng_deg, lng_min, lng_dir, dtw, btw, stw, wpt_status == WaypointArrived ? 'A' : 'V');
    outputLine(appendChecksum(sentence));
  }

  PilotStatusMessage::PilotStatusMessage() {
    wpt_id = "";
    xte_magnitude = 0.0;
    xte_units = Meters;
    steer = SteerLeft;
    wpt_status = WaypointNotSet;
  }
  
  void PilotStatusMessage::send() {
    char sentence[MaxSentence];
    if (wpt_status == WaypointNotSet) {
      fmtString(sentence, "$%sAPB,%c,%c,,,,%c,%c,,,,,,,*", TalkerID, 'A', 'A', 'V', 'V');
    } else {
      heading_to_steer = pos_to_dest_bearing;  // identical, since we don't simulate drift
      fmtString(sentence, "$%sAPB,%c,%c,%.1f,%c,%c,%c,%c,%.1f,%c,%s,%.1f,%c,%.1f,%c",
                TalkerID, 'A', 'A', xte_magnitude, steer, xte_units, 
                wpt_status == WaypointArrived ? 'A' : 'V', wpt_status == WaypointPassed ? 'A' : 'V',
                orig_to_dest_bearing, HeadingTrue, wpt_id, pos_to_dest_bearing, HeadingTrue, heading_to_steer, HeadingTrue);
    }
    outputLine(appendChecksum(sentence));
  }

} // end Simpilot namespace

#endif
