/*
  Description:
    Scanning/parsing functions used by Simpilot.
    The commmand function scans, parses and executes a simulation command.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>

#include "simpilot.h"

#define ERROR(MSG) { char buf[MaxMsg]; fmtString(buf, "Error: " MSG); outputLine(buf); return false; }
#define EXPECT_TYPE(TOK, TT) if ((TOK)->type != TT) { ERROR("Expected " #TT) }
#define EXPECT_CHAR(TOK, STR) if (strchr(STR, (TOK)->value.chr) == NULL) { ERROR("Expected one of " #STR) }
#define EXPECT_INT(TOK, LOWER, UPPER) if ((TOK)->value.integer < (LOWER) || (TOK)->value.integer > (UPPER)) { ERROR("Integer out of range") }
#define EXPECT_REAL(TOK, LOWER, UPPER) if ((TOK)->value.real < (LOWER) || (TOK)->value.real > (UPPER)) { ERROR("Number out of range") }

namespace Simpilot {
  #define MaxTokens 10

  enum TokenType {
    TokenTypeNull,
    TokenTypeKeyword,
    TokenTypeChar,
    TokenTypeInt,
    TokenTypeReal,
    TokenTypeId,
    TokenTypeDelim,
    TokenTypeComment
  };

  // NB: Keep both TokenKeyword and TokenKeywords in sync
  enum TokenKeyword {
    TokenAdd,
    TokenDistance,
    TokenGoto, 
    TokenHeading,
    TokenLogging,
    TokenMagVariation,
    TokenMode,
    TokenOff,
    TokenOn,
    TokenPeriod,
    TokenPosition,
    TokenReset, 
    TokenSet, 
    TokenSim,
    TokenSpeed,
    TokenTimescale,
    TokenUnits,
    TokenWaypoint,
    TokenMax
  };

  static const char * TokenKeywords[] = { 
    "add",
    "distance",
    "goto",
    "heading",
    "logging",
    "magvariation",
    "mode",
    "off",
    "on",
    "period",
    "position",
    "reset",
    "set",
    "sim",
    "speed",
    "timescale",
    "units",
    "waypoint"
  };
 
  struct Token {
    TokenType type;
    union {
      TokenKeyword keyword;
      char chr;
      int integer;
      double real;
      char id[MaxId];
    } value;
  };

  static bool scanCommand(const char * cmd, Token tokens[MaxTokens]) {
    // tokens types: keywords, chars, numbers (int, real) and separators
    // comments start with #
    Token* token_ptr = tokens;
    int token_cnt = 0;
    const char * cmd_ptr = cmd;
    
    token_ptr->type = TokenTypeNull;
    if (*cmd_ptr == '#') {
      token_ptr->type = TokenTypeComment; 
      return false;
    }
    
    while (*cmd_ptr == ' ') { // skip leading spaces
      cmd_ptr++;
    }
    while (*cmd_ptr != ';' && *cmd_ptr != '\0' && *cmd_ptr != '\r' && *cmd_ptr != '\n') {
      
      if (isalpha(*cmd_ptr)) {
        if (!(isalpha(*(cmd_ptr + 1)) || isdigit(*(cmd_ptr + 1)) || *(cmd_ptr + 1) == '_')) {
          // single character
          token_ptr->type = TokenTypeChar;
          token_ptr->value.chr = *cmd_ptr;
          cmd_ptr++;
          
        } else {
          // keyword
          int len;
          for (int ii = 0; ii < TokenMax; ii++) {
            len = strlen(TokenKeywords[ii]);
            if (strncmp(cmd_ptr, TokenKeywords[ii], len) == 0) {
              token_ptr->type = TokenTypeKeyword;
              token_ptr->value.keyword = (TokenKeyword)ii;
              cmd_ptr += len;
              break;
            }
          }
          // else identifier
          if (token_ptr->type == TokenTypeNull) {
            const char * id_ptr = cmd_ptr;
            while (isalpha(*cmd_ptr) || isdigit(*cmd_ptr) || *cmd_ptr == '_') {
              cmd_ptr++;
            }
            len = cmd_ptr - id_ptr;
            if (len >= MaxId) {
              len = MaxId - 1;
            }
            token_ptr->type = TokenTypeId;
            strncpy(token_ptr->value.id, id_ptr, len);
            token_ptr->value.id[len] = '\0';
          }
        }
        
      } else if (isdigit(*cmd_ptr) || *cmd_ptr == '.' || *cmd_ptr == '-') {
        // number
        const char * num_ptr = cmd_ptr;
        while (*cmd_ptr == '-' || *cmd_ptr == '.') {
          cmd_ptr++;
        }
        while (isdigit(*cmd_ptr)) {
          cmd_ptr++;
        }
        if (*cmd_ptr == '.') {
          cmd_ptr++;
          while (isdigit(*cmd_ptr)) { // consume fractional part of number
            cmd_ptr++;
          }
          token_ptr->type = TokenTypeReal;
          token_ptr->value.real = atof(num_ptr);
        } else {
          token_ptr->type = TokenTypeInt;
          token_ptr->value.integer = atoi(num_ptr);
        }
        
      } else if (*cmd_ptr == ',') {
        token_ptr->type = TokenTypeDelim;
        cmd_ptr++;
        
      } else {
        ERROR("Unexpected token");
      }
      
      while (*cmd_ptr == ' ') { // skip spaces
        cmd_ptr++;
      }
      token_ptr++;
      if (++token_cnt == MaxTokens) {
        return false; // too many tokens!
      }
      token_ptr->type = TokenTypeNull;
    }
    return true;
  }

  // parser  
  static bool simCommand(Simulation * sim, Token *token) {
    // parse & execute a "sim" command
    EXPECT_TYPE(token, TokenTypeKeyword);
    switch (token->value.keyword) {
    case TokenOn:
      sim->setRunning(true);
      return true;
      
    case TokenOff:
      sim->setRunning(false);
      return true;
      
    case TokenReset:
      sim->reset();
      return true;
      
    case TokenLogging:
      EXPECT_TYPE(++token, TokenTypeInt);
      sim->setLogLevel(token->value.integer);
      return true;
      
    case TokenTimescale:
      EXPECT_TYPE(++token, TokenTypeInt);
      sim->setTimescale(token->value.integer);
      return true;
      
    case TokenPeriod:
      EXPECT_TYPE(++token, TokenTypeInt);
      sim->setPeriod(token->value.integer);
      return true;
      
    case TokenUnits:
      EXPECT_TYPE(++token, TokenTypeKeyword);
      switch (token->value.keyword) {
      case TokenDistance:
        EXPECT_TYPE(++token, TokenTypeChar);
        EXPECT_CHAR(token, "NKM");
        sim->setDistanceUnits((DistanceUnits)token->value.chr);
        return true;
      case TokenSpeed:
        EXPECT_TYPE(++token, TokenTypeChar);
        EXPECT_CHAR(token, "NKM");
        sim->setSpeedUnits((SpeedUnits)token->value.chr);
        return true;
      case TokenHeading:
        EXPECT_TYPE(++token, TokenTypeChar);
        EXPECT_CHAR(token, "MT");
        sim->setHeadingUnits((HeadingUnits)token->value.chr);
        return true;
      default:
        ERROR("Expected keyword (distance, speed or heading) after units.");
      }
      
    default:
      ERROR("Expected keyword (run, protocol, logging, timescale, period or units) after sim.");
    }
    return true;
  }
  
  static bool setCommand(Simulation * sim, Token *token) {
    // parse & execute a "set" command
    EXPECT_TYPE(token, TokenTypeKeyword);
    switch (token->value.keyword) {
    case TokenMode:
      EXPECT_TYPE(++token, TokenTypeChar);
      EXPECT_CHAR(token, "SHWTVL");
      sim->setMode((PilotMode)token->value.chr);
      return true;

    case TokenPosition:
      token++;
      switch (token->type) {
      case TokenTypeReal:
        double lat, lng;
        EXPECT_REAL(token, -90, 90);
        lat = token->value.real;
        EXPECT_TYPE(++token, TokenTypeDelim);
        EXPECT_TYPE(++token, TokenTypeReal);
        EXPECT_REAL(token, -180, 180);
        lng = token->value.real;
        sim->setPosition(lat, lng);
        return true;
      case TokenTypeId:
        Waypoint * wpt;
        if (!sim->getWaypoint(token->value.id, -1, &wpt)) {
          ERROR("Expected waypoint ID after set position.");
        }
        sim->setPositionToWaypoint(wpt);
        return true;
      default:
        ERROR("Expected latitude,longitude or waypoint ID after position.");
      }

    case TokenSpeed:
      token++;
      switch (token->type) {
      case TokenTypeInt:
        EXPECT_INT(token, 0, INT_MAX);
        sim->setSpeed((double)token->value.integer);
        return true;
      case TokenTypeReal:
        EXPECT_REAL(token, 0, INT_MAX);
        sim->setSpeed(token->value.real);
        return true;
      default:
        ERROR("Expected number after speed.");
      }

    case TokenHeading:
      token++;
      switch (token->type) {
      case TokenTypeInt:
        EXPECT_INT(token, 0, 360);
        sim->setHeading((double)token->value.integer);
        return true;
      case TokenTypeReal:
        EXPECT_REAL(token, 0, 360);
        sim->setHeading(token->value.real);
        return true;
      default:
        ERROR("Expected number after heading.");
      }

    case TokenMagVariation:
      EXPECT_TYPE(++token, TokenTypeReal);
      EXPECT_REAL(token, -15, 15);
      sim->setMagVariation(token->value.real);
      return true;

    default:
      ERROR("Expected keyword (postion, speed, heading, or magvariation) after set.");
    }
  }

  static bool addCommand(Simulation * sim, Token *token) {
    // parse & execute an "add" command
    EXPECT_TYPE(token, TokenTypeKeyword);
    switch (token->value.keyword) {
    case TokenWaypoint:
      char * id;
      double lat, lng;
      EXPECT_TYPE(++token, TokenTypeId);
      id = token->value.id;
      EXPECT_TYPE(++token, TokenTypeDelim);
      EXPECT_TYPE(++token, TokenTypeReal);
      EXPECT_REAL(token, -90, 90);
      lat = token->value.real;
      EXPECT_TYPE(++token, TokenTypeDelim);
      EXPECT_TYPE(++token, TokenTypeReal);
      EXPECT_REAL(token, -180, 180);
      lng = token->value.real;
      return sim->addWaypoint(id, lat, lng);

    default:
      ERROR("Expected keyword (waypoint) after add.");
    }
  }

  static bool gotoCommand(Simulation * sim, Token *token) {
    // parse & execute a "goto" command
    EXPECT_TYPE(token, TokenTypeKeyword);
    switch (token->value.keyword) {
    case TokenWaypoint:
      EXPECT_TYPE(++token, TokenTypeId);
      return sim->gotoWaypoint(token->value.id, -1);
    case TokenOff:
      return sim->gotoWaypoint(NULL, -1);
    default:
      ERROR("Expected keyword (waypoint or off) after goto.");
    }
  }

  bool command(Simulation * sim, const char * cmd) {
    // scan, parse and execute a simulation command
    Token tokens[MaxTokens];
    Token* token = tokens;

    if (!scanCommand(cmd, tokens)) {
      EXPECT_TYPE(token, TokenTypeComment);
      return true;
    }
    // parse and execute the command
    EXPECT_TYPE(token, TokenTypeKeyword);
    switch (token->value.keyword) {
    case TokenSim:
      return simCommand(sim, ++token);
    case TokenSet:
      return setCommand(sim, ++token);
    case TokenAdd:
      return addCommand(sim, ++token);
    case TokenGoto:
      return gotoCommand(sim, ++token);
    default:
      ERROR("Expected sim, set, add or goto command");
    }
  }  

} // end Simpilot namespace
