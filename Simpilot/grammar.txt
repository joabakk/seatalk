; Simpilot grammar in BNF syntax
<digit> ::= '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9'
<alpha> ::= 'A' | 'B' | 'C' | 'D' | 'E' | 'F' | 'G' | 'H' | 'I' | 'J' | 'K' | 'L' | 'M' | 'N' | 'O' | 'P' | 'Q' | 'R' | 'S' | 'T' | 'U' | 'V' | 'W' | 'X' | 'Y' | 'Z' | 'a' | 'b' | 'c' | 'd' | 'e' | 'f' | 'g' | 'h' | 'i' | 'j' | 'k' | 'l' | 'm' | 'n' | 'o' | 'p' | 'q' | 'r' | 's' | 't' | 'u' | 'v' | 'w' | 'x' | 'y' | 'z'
<id-alpha> ::= <alpha> | '_'
<natural-number> ::= <digit> | <natural-number> <digit>
<integer-number> ::= ['-'] <natural-number>
<real-number> ::= ['-'] <natural-number> '.' <natural-number>
<number> ::= <integer-number> | <real-number>
<id> ::= <id-alpha> | <id> <id-alpha> | <id> <digit>
<latlng> ::= <real-number> ',' <real-number>

<sim-state> ::= 'on' | 'off' | 'reset'
<log-level> ::= '0' | '1' | '2' | '3' | '4'
<sim-param> ::= 'logging' <log-level> | 'timescale' <natural-number> | 'period' <natural-number>
<distance-units> ::= 'N' | 'K' | 'M'
<speed-units> ::= 'N' | 'K' | 'M'
<heading-units> ::= 'M' | 'T'
<sim-units> ::= 'distance' <distance-units> | 'speed' <speed-units> | 'heading' <heading-units>
<sim-command> ::= 'sim' <sim-state > | 'sim' <sim-param> | 'sim' 'units' <sim-units>

<waypoint> ::= <id> ',' <latlng>
<add-command> ::= 'add' 'waypoint' <waypoint>

<position> ::= <latlng> | <id>
<set-param> ::= 'position' <position> | 'speed' <number> | 'heading' <number> | 'magvariation' <number>
<set-command> ::= 'set' <set-param>

<goto-command> ::= 'goto' <id> | 'goto' 'off'

<command> ::= <sim-command> | <add-command> | <set-command> | <goto-command>
