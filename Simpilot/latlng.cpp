/*
  Description:
    Geospatial functions used by Simpilot.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <math.h>
#include "latlng.h"

namespace Geo {
  static const double EarthRadius = 6371000;

  // conversions
  double toRadians(double deg) {
    return deg * M_PI / 180;
  }

  double fromRadians(double rad) {
    return rad / M_PI * 180;
  }

  double toMeters(double distance, char units) {
    switch(units) {
    case 'N':
      return distance * 1852;
    case 'K':
      return distance * 1000;
    case 'M':
    default:
      return distance;
    }
  }

  double fromMeters(double distance, char units) {
    switch(units) {
    case 'N':
      return distance / 1852;
    case 'K':
      return distance / 1000;
    case 'M':
    default:
      return distance;
    }
  }

  double toMps(double speed, char units) {
    switch (units) {
    case 'N': // knots
      return speed * 0.51444;
    case 'K': // kph
      return speed * 0.27778;
    case 'M':
    default:
      return speed;
    }
  }

  double fromMps(double speed, char units) {
    switch (units) {
    case 'N': // knots
      return speed / 0.51444;
    case 'K': // kph
      return speed / 0.27778;
    case 'M':
    default:
      return speed;
    }
  }

  void LatLng::parts(int* lat_deg, double* lat_min, char* lat_dir, int* lng_deg, double* lng_min, char* lng_dir) {
    // split LatLng into pair of triplets: absolute whole degrees, fractional minutes and N/S/E/W direction
    if (lat < 0) {
      *lat_deg = -(int)lat;
      *lat_min = 60 * (-(*lat_deg) - lat);
      *lat_dir = 'S';
    } else {
      *lat_deg = (int)lat;
      *lat_min = 60 * (lat - *lat_deg);
      *lat_dir = 'N';
    }
    if (lng < 0) {
      *lng_deg = -(int)lng;
      *lng_min = 60 * (-(*lng_deg) - lng);
      *lng_dir = 'W';
    } else {
      *lng_deg = (int)lng;
      *lng_min = 60 * (lng - *lng_deg);
      *lng_dir = 'E';
    }
  }

  double LatLng::distance(LatLng& other) {
    // calculate distance to another latlng using Haversine formula
    double lat1 = toRadians(lat);
    double lat2 = toRadians(other.lat);
    double d_lat = toRadians(other.lat - lat);
    double d_lng = toRadians(other.lng - lng);
    double aa = sin(d_lat/2)*sin(d_lat/2) + cos(lat1)*cos(lat2)*sin(d_lng/2)*sin(d_lng/2);
    double cc = 2 * atan2(sqrt(aa), sqrt(1-aa));
    return cc * EarthRadius;
  }

  double LatLng::crossTrackDistance(LatLng& orig, LatLng& dest) {
    // calculate cross-track distance (cross-track error) for given point on a track between 
    // the given orig and dest points
    double orig_to_pos_dist = orig.distance(*this);
    double orig_to_pos_brng = orig.bearing(*this);
    double orig_to_dest_brng = orig.bearing(dest); 
    return asin(sin(orig_to_pos_dist/EarthRadius) * sin(orig_to_pos_brng-orig_to_dest_brng)) * EarthRadius;
  }

  double LatLng::bearing(LatLng& other) {
    // calculate initial (azimuth) bearing to another latlng
    double lat1 = toRadians(lat);
    double lat2 = toRadians(other.lat);
    double d_lng = toRadians(other.lng - lng);
    double bearing = fromRadians(atan2(sin(d_lng)*cos(lat2), cos(lat1)*sin(lat2)-sin(lat1)*cos(lat2)*cos(d_lng)));
    return fmod(bearing + 360, 360);
  }

  void LatLng::move(double distance, double heading) {
    // move a given distance on a given heading
    heading = toRadians(heading);
    double central_angle = distance/EarthRadius;
    double lat1 = toRadians(lat);
    double lng1 = toRadians(lng);
    double lat2 = asin(sin(lat1) * cos(central_angle) + cos(lat1) * sin(central_angle) * cos(heading));
    double lng2 = lng1 + atan2(sin(heading) * sin(central_angle)*cos(lat1), cos(central_angle) - sin(lat1)*sin(lat2));
    lat = fromRadians(lat2);
    lng = fromRadians(lng2);
  }

  bool LatLng::reaches(double distance, double heading, LatLng& target, int radius) {
    // return true if we would reach a target after moving the given distance on the given heading
    double dist = this->distance(target);
    if (distance < dist - radius) {
      // we fall short if the supplied distance is less than the distance to the target circle
      return false;
    }
    // find the points on the line through the target that is perpendicular to the heading and intersects the target circle
    LatLng lower_ll(target.lat, target.lng), upper_ll(target.lat, target.lng);
    upper_ll.move(radius, fmod(heading + 90, 360));
    double upper_brng = this->bearing(upper_ll);
    lower_ll.move(radius, fmod(heading - 90, 360));
    double lower_brng = this->bearing(lower_ll); 
    if (lower_brng > upper_brng) {
      // straddle true north
      lower_brng -= 360; 
      if (heading > 0) heading -= 360;
    }
    // the heading must lie between the 2 tangent bearings, otherwise we miss the target circle
    return (lower_brng <= heading && heading <= upper_brng);
  }

} // end Geo namespace

