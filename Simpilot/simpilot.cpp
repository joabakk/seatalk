/*
  Description:
    Simpilot - a simple boat autopilot simulator for Arduino

    Read the Readme for an overview.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <limits.h>
#include <stdarg.h>

#include "simpilot.h"

namespace Simpilot {
  Simulation::Simulation() : _orig_position(0,0), _current_position(0,0) {
    // state
    _pilot_mode = PilotStandby;
    _total_time = 0;
    _total_distance = 0; // in meters
    _init_position = NULL;
    _current_speed = DefaultSpeed;
    _current_heading = 0.0;
    _mag_variation = 0.0;
    _init_waypoint = NULL;
    _current_waypoint = NULL;
    _waypoint_status = WaypointNotSet;
    _waypoints = NULL;
    _n_waypoints = 0;
    _running = false;

    // config
#if defined(NMEA0183)
    _protocol = "nmea0183";
#elif defined(SEATALK)
    _protocol = "seatalk";
#else
    _protocol = "";
#endif
    _log_level = Error; // log errors only
    _timescale = 60;  // i.e., 1 sec of wallclock time = 1 min of simulated time
    _period = 5;      // update every 5 seconds (less frequent than this confuses OpenCPN)
    _distance_units = NauticalMiles;
    _speed_units = Knots;
    _heading_units = HeadingTrue;
  }

  int Simulation::getWaypointIndex(const char * id) {
    for (int ix = 0; ix < _n_waypoints; ix++) {
      if (strcmp(_waypoints[ix].id, id) == 0) {
        return ix;
      }
    }
    return -1;
  }

  bool Simulation::addWaypoint(const char * id, double lat, double lng) {
    if (getWaypointIndex(id) != -1) {
      LOG(Error, "Waypoint %s already exists", id);
      return false;
    }
    _waypoints = (Waypoint *)realloc((void *)_waypoints, (_n_waypoints + 1) * sizeof(Waypoint));
    Waypoint * wpt = (Waypoint *)(_waypoints) + _n_waypoints;
    strcpy(wpt->id, id); // guaranteed to be shorter than MaxId
    wpt->lat = lat;
    wpt->lng = lng;
    _n_waypoints++;
    LOG(Command, "add waypoint %s,%.6f,%.6f", id, lat, lng);
    return true;
  }

  bool Simulation::getWaypoint(const char * id, int ix, Waypoint ** wpt) {
    if (ix == -1) {
      ix = getWaypointIndex(id);
    }
    if (ix >= 0 && ix < _n_waypoints) {
      *wpt = _waypoints + ix;
      return true;
    }
    return false;
  }

  bool Simulation::gotoWaypoint(const char * id, int ix) {
    // setting a goto sets the origin, heading, and sets the status to WaypointActive
    // if the pilot in Standby or Heading mode, the mode is set to WaypointMode, otherwise it is left as is
    // stopping a goto sets the status to WaypointNotSent and switches to the Heading mode unless in Standby mode
    if (id == NULL) {
      _current_waypoint = NULL;
      _waypoint_status = WaypointNotSet;
      LOG(Command, "goto off");
      if (_pilot_mode != PilotStandby) {
        setMode(PilotHeading);
      }
      return true;
    }
    if (!getWaypoint(id, ix, &_current_waypoint)) {
      LOG(Error, "Invalid waypoint ID %s", id);
      return false;
    }
    if (_init_waypoint == NULL) {
      _init_waypoint = _current_waypoint;
    }
    _orig_position = _current_position;
    _waypoint_status = WaypointActive;
    Geo::LatLng dest(_current_waypoint->lat, _current_waypoint->lng);
    double dtw = Geo::fromMeters(_current_position.distance(dest), _distance_units);
    double btw = _current_position.bearing(dest);
    _current_heading = btw;
    LOG(Command, "goto waypoint %s", _current_waypoint->id);
    if (_pilot_mode == PilotStandby || _pilot_mode == PilotHeading) {
      setMode(PilotWaypoint);
    }
    LOG(Info, "WPT=%s, BTW=%.1f, DTW=%.1f, MOD=%c", _current_waypoint->id, btw, dtw, _pilot_mode);
    return true;
  }

  bool Simulation::nextWaypoint(const char * id) {
    // advance to the next waypoint, or set status to WaypointNotSet if none
    int ix = getWaypointIndex(id);
    if (ix == -1) {
      LOG(Error, "Invalid waypoint index %d", ix);
      return false;
    }
    if (ix < _n_waypoints - 1) {
      return gotoWaypoint("", ix + 1);
    } else {
      _waypoint_status = WaypointNotSet;
      return false;
    }
  }
    
  bool Simulation::waypointPassed(double distance) {
    // calculate if current waypoint will be passed if we move this distance on the current heading
    if (_current_waypoint == NULL) {
      LOG(Warning, "Calling waypointPassed without a waypoint");
      return false;
    }
    Geo::LatLng wpt(_current_waypoint->lat, _current_waypoint->lng);
    if (_current_position.reaches(distance, _current_heading, wpt, DefaultArrivalRadius)) {
      LOG(Debug, "waypoint %s passed", _current_waypoint->id);
      return true;
    } else {
      return false;
    }
  }

  void Simulation::maintainHeading(double* time, double* distance) {
    // move at current speed on current heading for given time, updating current position
    double speed = Geo::toMps(_current_speed, _speed_units);
    *distance = *time * speed;

    if (_waypoint_status == WaypointActive && waypointPassed(*distance)) {
      _waypoint_status = WaypointPassed;
    }
    _current_position.move(*distance, _current_heading);
  }

  void Simulation::maintainTrack(double* time, double* distance) {
    // track to current waypoint at current speed for given time, but don't pass it 
    // update current position and current heading; the latter is always the heading to the current waypoint
    if (_current_waypoint == NULL) {
      maintainHeading(time, distance);
      return;
    }
    Geo::LatLng wpt(_current_waypoint->lat, _current_waypoint->lng);
    _current_heading = _current_position.bearing(wpt);
    double dtw = _current_position.distance(wpt);
    double speed = Geo::toMps(_current_speed, _speed_units);
    *distance = *time * speed;

    if (*distance >= dtw) {
      *distance = dtw;
      *time = dtw / _current_speed;
      _current_position.set(_current_waypoint->lat, _current_waypoint->lng);
      _waypoint_status = WaypointArrived;
      LOG(Debug, "waypoint arrived (%ds since last update)", (long)(*time));
    } else {
      _current_position.move(*distance, _current_heading);
      _waypoint_status = WaypointActive;
    }
  }

  void Simulation::send() {
    int hrs, mins, secs;
    splitTime(_total_time, &hrs, &mins, &secs);
    double total_distance = Geo::fromMeters(_total_distance, _distance_units);
    LOG(Info, "TSS=%02d:%02d:%02d, POS=%.6f,%.6f, SOG=%.1f, HDT=%.1f, HDM=%.1f, DSS=%.1f", 
        hrs, mins, secs, _current_position.lat, _current_position.lng, _current_speed, 
        _current_heading, _current_heading + _mag_variation, total_distance);

    // always send environment, position and vector messages
    EnvMessage env_msg;
    env_msg.send();

    PositionMessage pos_msg;
    pos_msg.time = _total_time;
    pos_msg.position = _current_position;
    pos_msg.send();

    VectorMessage vector_msg;
    vector_msg.speed = _current_speed;
    vector_msg.speed_units = _speed_units;
    vector_msg.heading = _current_heading;
    vector_msg.mag_heading = _current_heading + _mag_variation;
    vector_msg.send();

    // send nav message whenever waypoint is active
    if (_waypoint_status == WaypointActive) {
      NavMessage nav_msg;
      // ToDo: calculate XTE
      Geo::LatLng dest(_current_waypoint->lat, _current_waypoint->lng);
      nav_msg.wpt_status = _waypoint_status;
      nav_msg.orig = _orig_position;
      nav_msg.dest = dest;
      nav_msg.dest_id = _current_waypoint->id;
      nav_msg.dtw = Geo::fromMeters(_current_position.distance(dest), _distance_units);
      nav_msg.btw = _current_position.bearing(dest);
      nav_msg.stw = _current_speed;
      nav_msg.send();
    }

    // send pilot message whenever autopilot is on (i.e., not standby)
    if (_pilot_mode != PilotStandby) {
      PilotStatusMessage pilot_msg;
      pilot_msg.mode = _pilot_mode;
      pilot_msg.wpt_status = _waypoint_status;
      pilot_msg.heading = _current_heading;
      if (_waypoint_status == WaypointNotSet) {
        pilot_msg.wpt_id = "";
      } else {
        pilot_msg.wpt_id = _current_waypoint->id;
        Geo::LatLng dest(_current_waypoint->lat, _current_waypoint->lng);
        pilot_msg.orig_to_dest_bearing = _orig_position.bearing(dest);
        pilot_msg.pos_to_dest_bearing = _current_position.bearing(dest);
      }
      pilot_msg.send();
    }
  }

  void Simulation::loop() {
    // This is the guts of the simulation: reads new commands, updates position and status, and sends messages
    // NB: this function does not loop itself but is meant to be called repeatedly _from_ a loop (just like Arduino's loop).

    PilotMode pilot_mode = _pilot_mode;
    char cmd[MaxCommand];
    if (inputAvailable()) {
      inputLine(cmd, MaxCommand);
      LOG(Debug, "%s", cmd);
      command(this, cmd); // ToDo: retry until valid command
    }
    if (!running()) {
      return; // simulation stopped/paused
    }
    if (_pilot_mode != pilot_mode) {
      LOG(Debug, "pilot mode change to %c", _pilot_mode)
    }
 
    delay(_period * 1000);
    double time = _period * _timescale;
    double distance = 0; // in meters

    switch(_pilot_mode) {
    case PilotStandby:
    case PilotHeading:
    case PilotWindvane:
      // maintain current heading at current speed
      maintainHeading(&time, &distance);
      break;
        
    case PilotWaypoint:
    case PilotTrack:
    case PilotLoop:
      // maintain course towards current waypoint at current speed
      maintainTrack(&time, &distance);
      break;
    }
      
    _total_time += time;    
    _total_distance += distance;
    send();

    switch(_pilot_mode) {
    case PilotStandby:
    case PilotHeading:
    case PilotWindvane:
      return; // maintain course

    case PilotWaypoint:
      if (_waypoint_status == WaypointActive) return; // continue tracking
      LOG(Debug, "waypoint complete");
      break;

    case PilotTrack:
    case PilotLoop:
      if ((_waypoint_status == WaypointActive) ||
	  ( _waypoint_status == WaypointArrived && nextWaypoint(_current_waypoint->id))) return; // continue tracking
      LOG(Debug, "track complete");
      if (_pilot_mode == PilotLoop) {
	LOG(Debug, "looping");
	gotoWaypoint("", 0);
	return;
      }
      break;
    }

    // we get here if there are no more waypoints and we're not looping
    send();
    setRunning(false);
  }

} // end Simpilot namespace

#ifdef MAIN
#include <stdio.h>

int main() {
  #ifdef TEST
  test();
  #endif
  // read commands from stdin
  char* line = NULL;
  size_t line_max = 0;
  ssize_t line_len;
  Simpilot::Simulation sim;
  while ((line_len = getline(&line, &line_max, stdin)) > 0) {
    command(&sim, line);
  }
  while (sim.running()) {
    sim.loop();
  }
  return 0;
}

#else
Simpilot::Simulation sim;

void loop() {
  sim.loop();
}
#endif
