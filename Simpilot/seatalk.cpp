/*
  Description:
    Seatalk message implementations.
    Define SEATALK in port.h to compile.
    SeaTalk electrical signals are non-inverted (i.e., TTL-like), so OK to use Arduino TX output as is.
    SeaTalk uses 9 bits per byte, so the 9-bit version of HardwareSerial is required.
    Seatalk Reference: http://www.thomasknauf.de/rap/seatalk2.htm
    Datagram variable names reflect those used by Knauf, except nibbles are suffixed with 4, i.e., z4, not z.
    Setting logging to 0 so only Seatalk datagrams get output.

  License:
    Copyright (C) 2016 Alan Noble.

    This file is part of Simpilot. Simpilot is free software: you can
    redistribute it and/or modify it under the terms of the GNU
    General Public License as published by the Free Software
    Foundation, either version 3 of the License, or (at your option)
    any later version.

    Simpilot is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Simpilot in gpl.txt.  If not, see
    <http://www.gnu.org/licenses/>.
*/
#include <stdlib.h>
#include <string.h>

#include "simpilot.h"

#ifdef SEATALK
namespace Simpilot {

  // We specify Seatalk datagrams as arrays of alternating field types and field values, terminated by End.
  // NB: Nibbles must be followed by a second nibble & order is most significant nibble first.
  // NB: Byte arrays must be followed by an integer representing the number of bytes
  enum SeatalkFieldType {
    End =    0,     // terminates a datagram specification spec
    Cmd =    1,     // command byte
    Att =    2,     // attribute byte
    Nibble = 3,     // data nibble (4 bits)
    Byte =   4,     // data byte (8 bits)
    Int =    5,     // data int (16 bits)
    Int10 =  6,     // data int scaled by 10 (NOT 10 ints)
    Bytes =  7      // byte array
  };

  static bool writeDatagram(long data[]) {
    // send a datagram; last element of data must be End
    for (int ii = 0; ; ii += 2) {
      switch (data[ii]) {
      case End:
        outputFlush();
        return true;

      case Cmd:
        // NB: set command bit
        outputByte(data[ii + 1] | 0x0100);     
        break;

      case Nibble:
        // NB: nibbles must come in pairs, with most significant first
        if (data[ii + 2] != Nibble) return false;
        outputByte((data[ii + 1] << 4) | (data[ii + 3] & 0x0f));
        ii += 2; // skip over next Nibble which we've already consumed
        break;

      case Att:
      case Byte:
        outputByte(data[ii + 1] & 0x00ff);     // mask high byte
        break;

      case Int:
      case Int10:
        outputByte(data[ii + 1] & 0x00ff);    // LSB first
        outputByte(data[ii + 1]  >> 8);       // MSB second
        break;

      case Bytes:
        if (data[ii + 2] == Int) {
          int len = data[ii + 2];
          char * str = (char*)(data[ii + 1]);
          while (--len >= 0) {
            outputByte(*str++);
          }
          ii += 2; // skip over next Int which we've already consumed
        } else {
          return false;
        }
        break;

      default:
        return false;
      }
    }
    return true;
  }

  static void splitHeading(double heading, int* hdg90, int* hdg2, int* hdg1) {
    int hdg = (int)heading;
    *hdg90 = hdg / 90;
    *hdg2 = (hdg % 90) / 2;
    *hdg1 = hdg & 0x1;
  }

  EnvMessage::EnvMessage() {
    depth = DefaultDepth;
    tws = DefaultTWS;
    aws = DefaultAWS;
  }
  
  void EnvMessage::send() {
    // send depth
    int y4 = 4; // units = meters
    int xx = (int)(M_TO_FT(depth * 10));
    long dbt_msg[] = { Cmd, 0x00, Att, 0x02, Nibble, y4, Nibble, 0, Int10, xx, End };
    writeDatagram(dbt_msg);

    // send AWS
    xx = (int)(aws * 10);
    int yy = xx % 10;
    xx /= 10;
    long aws_msg[] = { Cmd, 0x11, Att, 0x01, Byte, xx, Nibble, yy, Nibble, 0, End };
    writeDatagram(aws_msg);
  }

  PositionMessage::PositionMessage() {
  }
  
  void PositionMessage::send() {
    int lat_deg, lng_deg;
    double lat_min, lng_min;
    char lat_dir, lng_dir;
    position.parts(&lat_deg, &lat_min, &lat_dir, &lng_deg, &lng_min, &lng_dir);
    int z4 = 0;
    if (lat_dir == 'S') z4 |= 0x1;
    if (lng_dir == 'E') z4 |= 0x2;
    int xx = (int)(lat_min * 1000);
    int yy = xx % 256;
    xx /= 256;
    int qq = (int)(lng_min * 1000);
    int rr = qq % 256;
    qq /= 256;
    long pos_msg[] = { Cmd, 0x58, Nibble, z4, Nibble, 0x5, Byte, lat_deg, Byte, xx, Byte, yy, 
                       Byte, lng_deg, Byte, qq, Byte, rr, End };
    writeDatagram(pos_msg);
  }
  
  VectorMessage::VectorMessage() {
  }

  void VectorMessage::send() {
    // send SOG and HDG messsages.
    double mps = Geo::toMps(speed, speed_units);
    double knots = Geo::fromMps(mps, Knots);
    long sog_msg[] = { Cmd, 0x52, Att, 0x01, Int10, (int)(knots * 10), End };
    writeDatagram(sog_msg);

    int hdg90, hdg2, hdg1;
    splitHeading(heading, &hdg90, &hdg2, &hdg1);
    int u4 = (hdg1 << 2) | hdg90;
    int vw = hdg2;
    long hdg_msg[] = { Cmd, 0x89, Nibble, u4, Nibble, 0x2, Byte, vw, Byte, 0, Nibble, 0x2, Nibble, 0, End };
    writeDatagram(hdg_msg);
  }

  NavMessage::NavMessage() {
    xte_magnitude = 0.0;
    xte_units = NauticalMiles;
    steer = SteerRight;
    orig_id = "";
    dest_id = "";
    wpt_status = WaypointActive;
  }
  
  void NavMessage::send() {
    // ToDo: calculate XTE
    // NB: Seatalk requires null-padded strings
    char s1[] = "\0\0\0\0";
    char s2[] = "\0\0\0\0\0\0\0\0";
    char s3[] = "\0\0\0\0\0\0\0\0";

    size_t len = strlen(dest_id);
    if (len <= 4) {
      strncpy(s1, dest_id, len);
    } else {
      strncpy(s1, dest_id + len - 4, 4);
    }
    int x4 = 0;
    if (len <= 8) {
      strncpy(s2, dest_id, len);
    } else {
      x4 = 1;
      strncpy(s2, dest_id, 8);
      strncpy(s3, dest_id + 8, MAX(len - 8, 8));
    }
    long wpt_msg[] = { Cmd, 0xA1, Nibble, x4, Nibble, 0xD, Byte, 0x49, Byte, 0x49, 
                       Bytes, (long)s1, Int, 4, Bytes, (long)s2, Int, 8, End };
    writeDatagram(wpt_msg);
    if (x4 == 1) {
      // names longer than 8 bytes require a 2nd record; Simpilot does not support names longer than 16 bytes
      wpt_msg[3] = 3;
      wpt_msg[15] = (long)s3;
      writeDatagram(wpt_msg);
    }
  }

  PilotStatusMessage::PilotStatusMessage() {
    wpt_id = "";
    xte_magnitude = 0.0;
    xte_units = Meters;
    steer = SteerLeft;
    wpt_status = WaypointNotSet;
  }
  
  void PilotStatusMessage::send() {
    // mode
    int z4;
    switch(mode) {
    case PilotStandby:
      z4 = 0x0;
      break;
    case PilotHeading: // "Auto" mode
      z4 = 0x2; 
      break;
    case PilotWindvane:
      z4 = 0x4;
      break;
    case PilotWaypoint:
    case PilotTrack:
      z4 = 0x8;
      break;
    }
    // vessel heading
    int hdg90, hdg2, hdg1;
    splitHeading(heading, &hdg90, &hdg2, &hdg1);
    // course (dest) bearing
    int brng = (int)pos_to_dest_bearing;
    int brng90 = brng / 90;
    int xy = (brng % 90) * 2;
    // steering
    int right = (steer == SteerRight) ? 1 : 0;
    
    int u4 = (right << 3) | (hdg1 << 2) | hdg90;
    int vw = (brng90 << 6) | hdg2;
    int m4 = 0; // set to 0x8 for wind shift alarm

    long ps_msg[] = { Cmd, 0x84, Nibble, u4, Nibble, 6, Byte, vw, Byte, xy, Nibble, 0, Nibble, z4,
                      Nibble, 0, Nibble, m4, Byte, 0, Byte, 0, Byte, 0, End };
    writeDatagram(ps_msg);
  }

} // end Simpilot namespace
#endif
