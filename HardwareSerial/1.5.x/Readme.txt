Readme

This directory contains bouni's modified version of HardwareSerial for 9-bit serial communications.
This versions works for versions 1.5.x and 1.6.x of the Arduino IDE.
See this blog post for more info: http://blog.bouni.de/blog/2014/10/07/sending-and-receiving-9-bit-frames-with-arduino/
