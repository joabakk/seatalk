# Readme

Various Seatalk (and NMEA 0183) programs for Arduino and libaries to
support them.

# SeatalkTest

An Arduino test program to write Seatalk test messages. Start here if
you're working with Seatalk for the first time.

# Simpilot

A simple boat autopilot simulator for Arduino, which supports Seatalk
and NMEA 0183.

# HardwareSerial

Modified versions of HardwareSerial for 9-bit serial communications
required for Seatalk. You will need to manually replace the
HardwareSerial files that came with the Arduino IDE with these files.

# See Also

* [SeatalkTest blog post](http://blog.arribasail.com/2015/08/seatalk-using-arduino.html)
* [Simpilot  blog post](http://blog.arribasail.com/2016/10/tech-simpilot-arduino-boat-autopilot-simulator.html)
* [NMEA 0183 reference](http://freenmea.net/docs)
* [NMEA 0183 protocol](http://www.tronico.fi/OH6NT/docs/NMEA0183.pdf)
* [Seatalk reference](http://www.thomasknauf.de/rap/seatalk2.htm)

# License

SeatalkTest and Simpilot are Copyright (C) 2016 Alan Noble.

HardwareSerial is Copyright (C) 2006 Nicholas Zambetti.

They are free software: you can redistribute it and/or modify them
under the terms of the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.

They are distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with Simpilot in gpl.txt.  If not, see [GNU licenses](http://www.gnu.org/licenses).
